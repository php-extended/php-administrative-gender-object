# php-extended/php-administrative-gender-object
A library that implements the php-administrative-gender-interface library.

![coverage](https://gitlab.com/php-extended/php-administrative-gender-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-administrative-gender-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-administrative-gender-object ^8`


## License

MIT (See [license file](LICENSE)).
