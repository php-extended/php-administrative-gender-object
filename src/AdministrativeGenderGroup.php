<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Gender;

/**
 * AdministrativeGenderGroup class file.
 * 
 * This class is a simple implementation of the AdministrativeGenderGroupInterface.
 * 
 * @author Anastaszor
 */
class AdministrativeGenderGroup implements AdministrativeGenderGroupInterface
{
	
	/**
	 * The multiple known genders.
	 * 
	 * @var array<integer, AdministrativeGenderMultiplicity>
	 */
	protected array $_multiples = [];
	
	/**
	 * Builds a new AdministrativeGenderGroup with the given genders.
	 * 
	 * @param array<integer, AdministrativeGender> $genders
	 */
	public function __construct(array $genders = [])
	{
		foreach($genders as $gender)
		{
			$this->absorb($gender);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return \implode(',', \array_map(function(AdministrativeGenderMultiplicity $multiplicity)
		{
			return $multiplicity->__toString();
		}, $this->_multiples));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderGroupInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === \count($this->_multiples);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderGroupInterface::contains()
	 */
	public function contains(AdministrativeGenderInterface $gender) : bool
	{
		foreach($this->_multiples as $multiplicity)
		{
			if($multiplicity->containsGender($gender))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderGroupInterface::containsMultiple()
	 */
	public function containsMultiple(AdministrativeGenderInterface $gender) : bool
	{
		foreach($this->_multiples as $multiplicity)
		{
			if($multiplicity->containsGender($gender) && $multiplicity->isMultiple())
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderGroupInterface::absorb()
	 */
	public function absorb(AdministrativeGenderInterface $gender) : bool
	{
		$newMultiples = [];
		
		foreach($this->_multiples as $multiplicity)
		{
			if($multiplicity->containsGender($gender) && $gender instanceof AdministrativeGender)
			{
				$newMultiples[] = new AdministrativeGenderMultiplicity($gender, true);
				continue;
			}

			$newMultiples[] = $multiplicity;
		}

		$this->_multiples = $newMultiples;
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderGroupInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function beVisitedBy(AdministrativeGenderGroupVisitorInterface $visitor)
	{
		if($this->isEmpty())
		{
			return $visitor->visitEmpty($this);
		}
		
		if($this->containsMultiple(AdministrativeGender::MALE) && $this->containsMultiple(AdministrativeGender::FEMALE))
		{
			return $visitor->visitMultipleMalesMultipleFemales($this);
		}
		
		if($this->containsMultiple(AdministrativeGender::MALE) && $this->contains(AdministrativeGender::FEMALE))
		{
			return $visitor->visitMultipleMalesOneFemale($this);
		}
		
		if($this->containsMultiple(AdministrativeGender::MALE))
		{
			return $visitor->visitMultipleMales($this);
		}
		
		if($this->containsMultiple(AdministrativeGender::FEMALE) && $this->contains(AdministrativeGender::MALE))
		{
			return $visitor->visitOneMaleMultipleFemales($this);
		}
		
		if($this->containsMultiple(AdministrativeGender::FEMALE))
		{
			return $visitor->visitMultipleFemales($this);
		}
		
		if($this->contains(AdministrativeGender::MALE) && $this->contains(AdministrativeGender::FEMALE))
		{
			return $visitor->visitOneMaleOneFemale($this);
		}
		
		if($this->contains(AdministrativeGender::MALE))
		{
			return $visitor->visitOneMale($this);
		}
		
		if($this->contains(AdministrativeGender::FEMALE))
		{
			return $visitor->visitOneFemale($this);
		}
		
		return $visitor->visitEmpty($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderGroupInterface::mergeWith()
	 */
	public function mergeWith(AdministrativeGenderGroupInterface $other) : AdministrativeGenderGroupInterface
	{
		$new = clone $other;
		
		foreach($this->_multiples as $multiplicity)
		{
			$new->absorb($multiplicity->getGender());
			if($multiplicity->isMultiple())
			{
				$new->absorb($multiplicity->getGender()); // twice to trigger mult
			}
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderGroupInterface::equals()
	 */
	public function equals($other) : bool
	{
		if(!$other instanceof AdministrativeGenderGroupInterface)
		{
			return false;
		}
		
		foreach($this->_multiples as $group)
		{
			if($group->isMultiple())
			{
				if(!$other->containsMultiple($group->getGender()))
				{
					return false;
				}
				continue;
			}
			
			if(!$other->contains($group->getGender()))
			{
				return false;
			}
		}
		
		return true;
	}
	
}
