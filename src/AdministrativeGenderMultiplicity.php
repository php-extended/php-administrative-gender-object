<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Gender;

use Stringable;

/**
 * AdministrativeGenderMultiplicity class file.
 * 
 * This class is an helper to manipulate multiples of the same gender.
 * 
 * @author Anastaszor
 */
class AdministrativeGenderMultiplicity implements Stringable
{
	
	/**
	 * The gender encapsulated by this object.
	 * 
	 * @var AdministrativeGender
	 */
	protected AdministrativeGender $_gender;
	
	/**
	 * Whether this multiplicity represents multiples of this gender.
	 * 
	 * @var boolean
	 */
	protected bool $_multiple = false;
	
	/**
	 * Builds a new AdministrativeGenderMultiplicity with the given gender and
	 * multiple indicator.
	 * 
	 * @param AdministrativeGender $gender
	 * @param boolean $multiple
	 */
	public function __construct(AdministrativeGender $gender, bool $multiple)
	{
		$this->_gender = $gender;
		$this->_multiple = $multiple;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_gender->value.($this->isMultiple() ? '+' : '');
	}
	
	/**
	 * Gets whether this gender multiplicity equals another gender multiplicity.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool
	{
		if($object instanceof self)
		{
			return $object->containsGender($this->_gender)
				&& $this->isMultiple() === $object->isMultiple();
		}
		
		return false;
	}
	
	/**
	 * Gets whether this gender represents multiple individuals.
	 * 
	 * @return boolean
	 */
	public function isMultiple() : bool
	{
		return $this->_multiple;
	}
	
	/**
	 * Gets the gender of this multiplicty.
	 * 
	 * @return AdministrativeGenderInterface
	 */
	public function getGender() : AdministrativeGenderInterface
	{
		return $this->_gender;
	}
	
	/**
	 * Gets whether this multiplicity represents the given gender.
	 * 
	 * @param AdministrativeGenderInterface $gender
	 * @return boolean
	 */
	public function containsGender(AdministrativeGenderInterface $gender)
	{
		return $this->_gender->equals($gender);
	}
	
}
