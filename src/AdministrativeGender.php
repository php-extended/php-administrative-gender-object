<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Gender;

/**
 * AdministrativeGender class file.
 * 
 * This class is a simple implementation of the AdministrativeGenderInterface.
 * 
 * @author Anastaszor
 */
enum AdministrativeGender : string implements AdministrativeGenderInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderInterface::isMale()
	 */
	public function isMale() : bool
	{
		return AdministrativeGender::MALE === $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderInterface::isFemale()
	 */
	public function isFemale() : bool
	{
		return AdministrativeGender::FEMALE === $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderInterface::isFemale()
	 */
	public function equals($other) : bool
	{
		return $this === $other;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(AdministrativeGenderVisitorInterface $visitor)
	{
		if($this->isMale())
		{
			return $visitor->visitMale($this);
		}
		
		if($this->isFemale())
		{
			return $visitor->visitFemale($this);
		}
		
		return $visitor->visitUnknown($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Gender\AdministrativeGenderInterface::mergeWith()
	 */
	public function mergeWith(AdministrativeGenderInterface $other) : AdministrativeGenderInterface
	{
		if(AdministrativeGender::UNKNOWN === $this)
		{
			return $other;
		}

		if($this === $other)
		{
			return $this;
		}
		
		return AdministrativeGender::MALE;
	}

	case MALE = 'M';
	case FEMALE = 'F';
	case UNKNOWN = 'U';
	
}
