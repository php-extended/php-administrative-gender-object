<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Gender\AdministrativeGender;
use PhpExtended\Gender\AdministrativeGenderMultiplicity;
use PHPUnit\Framework\TestCase;

/**
 * AdministrativeGenderMultiplicityTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Gender\AdministrativeGenderMultiplicity
 *
 * @internal
 *
 * @small
 */
class AdministrativeGenderMultiplicityTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AdministrativeGenderMultiplicity
	 */
	protected AdministrativeGenderMultiplicity $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('M+', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AdministrativeGenderMultiplicity(
			AdministrativeGender::MALE,
			true,
		);
	}
	
}
