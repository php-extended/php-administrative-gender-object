<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-administrative-gender-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Gender\AdministrativeGenderGroup;
use PHPUnit\Framework\TestCase;

/**
 * AdministrativeGenderGroupTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Gender\AdministrativeGenderGroup
 *
 * @internal
 *
 * @small
 */
class AdministrativeGenderGroupTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AdministrativeGenderGroup
	 */
	protected AdministrativeGenderGroup $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AdministrativeGenderGroup();
	}
	
}
